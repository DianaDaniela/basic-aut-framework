package tests;

import Utils.ExcelReader;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import models.AccountModel;
import models.LoginModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.LoginPage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.Reader;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import static Utils.OtherUtils.sanitizeNullDbString;

public class DataTests extends BaseUITest {
    @DataProvider(name = "jsonDp")
    public Iterator<Object[]> jsonDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        File f = new File("src\\test\\resources\\data\\testdata.json");
        LoginModel lm = mapper.readValue(f, LoginModel.class);
        dp.add(new Object[]{lm});
        return dp.iterator();
    }

    @DataProvider(name = "xmlDp")
    public Iterator<Object[]> xmlDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        File f = new File("src\\test\\resources\\data\\testdata.xml");
        LoginModel lm = unMarshallLoginModel(f);
        dp.add(new Object[]{lm});
        return dp.iterator();
    }

    private LoginModel unMarshallLoginModel(File f) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(LoginModel.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (LoginModel) jaxbUnmarshaller.unmarshal(f);
        } catch (JAXBException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @DataProvider(name = "csvDp")
    public Iterator<Object[]> csvDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<>();
        File f = new File("src\\test\\resources\\data\\testdata.csv");
        Reader reader = Files.newBufferedReader(Paths.get(f.getAbsolutePath()));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> csvData = csvReader.readAll();
        for (int i = 0; i < csvData.size(); i++) {
//            AccountModel ac = new AccountModel();
//            ac.setUsername(csvData.get(i)[0]);
//            ac.setPassword(csvData.get(i)[1]);
//            LoginModel lm = new LoginModel();
//            lm.setAccount(ac);
//            lm.setUserError(csvData.get(i)[2]);
//            lm.setPasswordError(csvData.get(i)[3]);
//            lm.setGeneralError(csvData.get(i)[4]);
            //dp.add(new Object[]{ lm }); // is being replaced by the following line due to constructor
            dp.add(new Object[]{new LoginModel(csvData.get(i)[0],
                    csvData.get(i)[1],
                    csvData.get(i)[2],
                    csvData.get(i)[3],
                    csvData.get(i)[4])});
        }
        return dp.iterator();
    }

    @DataProvider(name = "xlsxDp")
    public Iterator<Object[]> xlsxDpCollection() throws Exception {
        Collection<Object[]> dp = new ArrayList<>();
        File f = new File("src\\test\\resources\\data\\testdata.xlsx");
        String[][] excelData = ExcelReader.readExcelFile(f, "Sheet1", true, true);
        for (int i = 0; i < excelData.length; i++) {
            AccountModel am = new AccountModel();
            am.setUsername(excelData[i][0]);
            am.setPassword(excelData[i][1]);
            LoginModel lm = new LoginModel();
            lm.setAccount(am);
            lm.setUserError(excelData[i][2]);
            lm.setPasswordError(excelData[i][3]);
            lm.setGeneralError(excelData[i][4]);
            dp.add(new Object[]{lm});
        }

        return dp.iterator();
    }

    @DataProvider(name = "sqlDp")
    public Iterator<Object[]> sqlDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://" + dbHostname + ":" + dbPort + "/" + dbSchema,
                    dbUsername, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM automation.authentication;");
            while (results.next()) {
                AccountModel am = new AccountModel();
                am.setUsername(sanitizeNullDbString(results.getString("username")));
                am.setPassword(sanitizeNullDbString(results.getString("password")));
                LoginModel lm = new LoginModel();
                lm.setAccount(am);
                lm.setUserError(sanitizeNullDbString(results.getString("userError")));
                lm.setPasswordError(sanitizeNullDbString(results.getString("passwordError")));
                lm.setGeneralError(sanitizeNullDbString(results.getString("generalError")));
                dp.add(new Object[]{lm});
            }
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }

    @Test(dataProvider = "jsonDp")
    public void jsonTest(LoginModel lm) {
        printData(lm);
        loginActions(lm);
    }

    @Test(dataProvider = "xmlDp")
    public void xmlTest(LoginModel lm) {
        printData(lm);
        loginActions(lm);
    }

    @Test(dataProvider = "csvDp")
    public void csvTest(LoginModel lm) {
//        test = extent.createTest("CSV Login Test (" + lm.getAccount().getUsername() + "," + lm.getAccount().getPassword() + ")");
        printData(lm);
        loginActions(lm);
    }

    @Test(dataProvider = "xlsxDp")
    public void xlsxTest(LoginModel lm) {
//        test = extent.createTest("XLSX Login Test (" + lm.getAccount().getUsername() + "," + lm.getAccount().getPassword() + ")");
        printData(lm);
        loginActions(lm);
    }

    @Test(dataProvider = "sqlDp")
    public void sqlTest(LoginModel lm) {
        printData(lm);
        loginActions(lm);
    }

    private void printData(LoginModel lm) {
        System.out.println(lm.getAccount().getUsername());
        System.out.println(lm.getAccount().getPassword());
        System.out.println(lm.getUserError());
        System.out.println(lm.getPasswordError());
        System.out.println(lm.getGeneralError());
    }

    private void loginActions(LoginModel lm) {
        LoginPage lp = new LoginPage(driver);
//        logInfoStatus("Open the next hostname:" + hostname);
        lp.openLoginPage(hostname);
//        logInfoStatus("Log with account:" + lm.getAccount().getUsername() + ", password:" + lm.getAccount().getPassword());
        lp.login(lm.getAccount().getUsername(), lm.getAccount().getPassword());

//        logInfoStatus("Verify that userErr:" + lm.getUserError() + " is present on screen");
        Assert.assertTrue(lp.checkErr(lm.getUserError(), "userErr"));
        Assert.assertTrue(lp.checkErr(lm.getPasswordError(), "passErr"));
        Assert.assertTrue(lp.checkErr(lm.getGeneralError(), "generalErr"));
    }

}
