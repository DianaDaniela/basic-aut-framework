package tests;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.core.IsEqual.equalTo;

public class CountryNameApiTest extends BaseControllerTest {

    @Test
    public void getCountryNameInfo() {
        Response response =
                given().log().uri().
                        when().
                        get("/{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                        then().
                        statusCode(200).
                        contentType(JSON).
                        body("name[0]", equalTo("Romania")).
                        body("currencies[0][0].code", equalTo("RON")).
                        extract().
                        response();

        response.getBody().prettyPrint();
    }

    @Test
    public void verifyCountryCapital() {
        given().log().uri().
                when().
                get("/{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
                body("capital[0]", equalTo("Bucharest"));
    }

    @Test
    public void verifyBorders() {
        List<String> expectedBorders = Arrays.asList("BGR", "HUN", "MDA", "SRB", "UKR");
        List<String> borders = given().log().uri().
                when().
                get("/{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
//               here's the magic
        extract().jsonPath().getList("borders[0]", String.class);

        System.out.println("Actual borders:" + borders);

        Assert.assertTrue(Arrays.deepEquals(borders.toArray(), expectedBorders.toArray()));
    }

    @Test
    public void verifyAltSpellings(){
        given().log().uri().
                when().
                get("/{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
                body("altSpellings[0]", hasItem("România")).
                body("altSpellings[0]", hasItem("Rumania")).
                body("altSpellings[0]", hasItem("RO")).
                body("altSpellings[0]", hasItem("Roumania"));
    }

    @Test
    public void verifyMultipleCurrencies(){
        given().log().uri().
                when().
                get("/{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Antarctica").
                then().
                statusCode(200).
                contentType(JSON).
                body("currencies[0][0].symbol", equalTo("$")).
                body("currencies[0].symbol", hasItem("£"));
    }
}
