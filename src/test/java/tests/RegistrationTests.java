package tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.ProfilePage;
import pageObjects.RegistrationPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


public class RegistrationTests extends BaseUITest {
    @DataProvider(name = "registrationAccDp")
    public Iterator<Object[]> registrationAccDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // FirstName, LastName, Email, UserName, Password,ConfirmPassword
        dp.add(new String[]{"dana", "popa", "dana@yahoo.com", "dana", "password", "password"});
        return dp.iterator();
    }

    @Test(dataProvider = "registrationAccDp")
    public void registerPositive(String firstName, String lastName, String email, String userName, String password, String confirmPassword) {
        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.openRegisterPage(hostname);
        registrationPage.waitForRegistrationPage();
        registrationPage.register(firstName, lastName, email, userName, password, confirmPassword);

        ProfilePage profilePage = new ProfilePage(driver);
        System.out.println("Register in user :" + profilePage.getUser());
        Assert.assertEquals(firstName, profilePage.getUser());
        profilePage.logOut();
    }
}
