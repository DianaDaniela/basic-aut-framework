package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class RegistrationPage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "inputFirstName")
    WebElement firstnameInput;
    @FindBy(id = "inputLastName")
    WebElement lastnameInput;
    @FindBy(id = "inputEmail")
    WebElement emailInput;
    @FindBy(id = "inputUsername")
    WebElement usernameInput;
    @FindBy(id = "inputPassword")
    WebElement passwordInput;
    @FindBy(id = "inputPassword2")
    WebElement confirmPassInput;
    @FindBy(id = "register-submit")
    WebElement submitButton;


    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void register(String firstName, String lastName, String email, String userName, String password, String confirmPassword) {
        firstnameInput.clear();
        firstnameInput.sendKeys(firstName);
        lastnameInput.clear();
        lastnameInput.sendKeys(lastName);
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.clear();
        usernameInput.sendKeys(userName);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        confirmPassInput.clear();
        confirmPassInput.sendKeys(confirmPassword);
        submitButton.submit();
    }

    public void waitForRegistrationPage() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void openRegisterPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/stubs/auth.html#registration_panel");
        driver.get(hostname + "/stubs/auth.html#registration_panel");
        WebElement registration = driver.findElement(By.id("register-tab"));
        registration.click();
    }
}
